cmake_minimum_required(VERSION 2.8.9)
project(directory_test)
set(CMAKE_BUILD_TYPE Release)

include_directories(include)

#This pulls all the src files with minimal tomfuckery
file(GLOB SOURCES "src/*.cpp")


add_executable(testSystem $[SOURCES])
